*********************************
Ircbot-llama Installation Guide
*********************************
 
Instructions on how to install, run, and modify ircbot-llama, a python based IRC bot on linux.
ircbot-llama is a python based IRC bot with features and updates being added actively.

GitHub Repo: https://github.com/mikellama/ircbot-llama

Requirements
-------------
Requirements: pip, python, wikipedia, urbandictionary, imdbparser, wordnik.

API Key Requirements

?dict - Requires a wordnik API key. Information on how to obtain one is located further down.

?weather - Requires a WeatherUnderground API key. Information on how to obtain one coming soon.

.. note::

  This bot will be upgraded for Python 3+. That is on the list of things to be done so expect that. :)
 
Installing The Bot and Required Packages
-----------------------------------------

1. Clone the ircbot-llama repository.

  .. code:: bash

       $ git clone https://github.com/mikellama/ircbot-llama

2. Use pip to install the required packages. If you have do not have root access you need to add ``--user --upgrade`` to the end of the following commands(s). If you do not have pip or you are unsure what it is, refer to this link:


  .. code:: bash

       $ pip install wikipedia
       $ pip install urbandictionary
       $ pip install imdbparser
       $ pip install wordnik
      
Or

  .. code:: bash
  
       $ pip install -r http://web.detectivetaco.net/raw/ircbot-llama-requirements.txt --user --upgrade

3. Cd to the ircbot-llama directory.

  .. code:: bash

       $ cd ircbot-llama

Bot Settings
--------------------------
This section will guide you through configuring the bot, such as the network it connects to, IRC nick, NickServ password, bot admins, API keys, and more.

Changing the network
^^^^^^^^^^^^^^^^^^^^^^^^
The IRC network in which your bot will connect to is located in ``llamabot.py``. This is where you will put the host and port of the network you want the bot to connect to is located in the ``llamabot.py`` file.

1. Open the ``llamabot.py`` file.

  .. code:: bash

      $ nano llamabot.py
    
2. Scroll down and find this line in the file.

  .. code:: python

      sock.connect(("irc.freenode.net", 6667))
  
Change the network to the network you want the bot to connect to.
  
3. Save and exit the ``llamabot.py`` file.

Modifiying the details.py file
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The other information such as your bots IRC nick, NickServ password, bot admins, the required API keys, etc is located in the ``details.py`` file.
So of course you will want to modify the ``details.py`` file to fit your needs.

1. Open the details.py file.

  .. code:: bash

       $ nano details.py

The ``details.py`` file consists of the nick, username, channel, secret (NickServ password), bot admins, and API keys. 

2. Change the nick from the default nick to whatever nick you want your bot to use.

3. The username is the realname the bot will use on IRC. Change it to what you want the bot's realname to be.

4. Change the channel to the channel you want the bot to join. (I do not believe there is an option for multiple channels yet.)

5. Admins is the users who will have access to functions such as ?ignore. Change this of course. You can have multiple admins in the list by using this format.

  .. code:: python

      admins = ["john", "bob"]

Just keep the ``details.py`` file open because the next section will explain the API keys.

Adding the API keys
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Both the WordNik dictionary function and the weather functions require you to obtain an API key. 

The dictionary function on the bot requires you to have a WordNik API key. You may obtain one of those by going to https://wordnik.com and signing up for an account and requesting an API key. 

Obtaining a WeatherUnderground Key: COMING SOON

1. Open the ``details.py`` file. (Unless it is alredy open from the last section.)

  .. code:: bash

       $ nano details.py
      
2. Add these two lines to your ``details.py`` file if it is not already there. 

  .. code:: python

      wnAPI_key = "here"
      wuAPI_key = "here"

wnAPI_key is for your Wordnik api key. 
wuAPI_key is for your WeatherUnderground API key.
Replace here with the appropriate API keys.

3. Save and exit the ``details.py`` file.

Editing the mwaaa.py file (reply dictionary)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The reply dictionary is located in the ``mwaaa.py`` file. It is a customizable list of commands that you can make and the bot will respond with the response you set in the reply dictionary file.

1. Open the ``mwaaa.py`` file.

  .. code:: bash

       $ nano mwaaa.py

2. Scroll down and you will see multiple lines that look like this:

  .. code:: python

      reply["?example"]  =  "sentence you want ?example to reply with goes here”

3. You may replace as many or all of those lines with whatever you want.  For example: If you want the bot to respond with your websites url when the command ?website is sent. You would put

  .. code:: python

      reply["?website"]  =  "https://example.com"
     
4. Save and exit the ``mwaaa.py`` file after you have modified the file to your needs.

Running and maintaining the bot
--------------------------------

Starting the bot
^^^^^^^^^^^^^^^^^^^^^^
There are two different ways to start the bot.

First Way:

  .. code:: bash

       $ python llamabot.py

The issue with doing it this way is that it will stop the bot when you close the terminal window.

The second method will prevent this.

Second Way:

  .. code:: bash

       $ nohup python llamabot.py &

Updating the bot
^^^^^^^^^^^^^^^^^^^^^^^
Every once and a while you should check the ircbot-llama github repo to see if any new major updates or new features are added. If there are you should update the bot.

To update the bot to the latest commit from the mikellama/ircbot-llama repo you would do 

  .. code:: bash

       $ git pull

The files that will not be overwritten by this are ``details.py`` and ``mwaaa.py``
If you have made any changes to other files make sure to remember what they were and copy them into another file before you update the bot. I highly suggest making a copy of ircbot-llama before updating in the event that something goes wrong.

If you make changes to files that are not ``mwaaa.py`` and ``details.py`` I suggest only updating when it is a bug fix or a signifigant update so you do not have to go through the hassle of copying your changes into the new version.

Credits

``:github_url:https://github.com/mike-llama/ircbot-llama``

Mike - Bot Developer/Creator (https://github.com/mike-llama/)

DetectiveTaco - Documentation Author (https://github.com/TheDetectiveTaco)

