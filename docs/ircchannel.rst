TacoCentral IRC Channel
======================

This page contains information about the ##tacocentral IRC channel on Freenode.

What is the channel about?
---------------------------------------

##tacocentral is my vision of a social IRC channel were you have very few restrictions, were there is a fun chat environment, and no set topic for the chat. That is what ##tacocentral is about.



Who manages the channel?
--------------------------

DetectiveTaco: I own ##tacocentral and all websites and services associated with it.

duckgoose: My second staff member.


Default Bans
-------------

gateway/tor-sasl/* is banned by default because of the possibility for abuse. If you try joining ##tacocentral with a gateway/tor-sasl/* hostmask you will be kickbanned. Please private message DetectiveTaco or duckgoose for an exception. 


Bots
-------------

Sometimes we are open for people to have their bots present in our channel. If you would like to add your bot to ##tacocentral please check with DetectiveTaco or duckgoose first. Any unauthorized bots will be banned. We reserve the right to deny your bots access to our channel.

Bots run by me (DetectiveTaco)

[Taco] - Limnoria. Helps with channel management and does a few other things.

TacoTheBot - A fun bot that has many commands such as urban dictionary, wiki, and more! Check wiki.detectivetaco.tk for a list of commands.

TacoCentralBot - Eggdrop. Does channel stats, weather, and more. Check wiki.detectivetaco.tk for more information on commands.

Bots run by other users

bannon3002 (Lended to us and ran by Mahjong) - CloudBot. Does many things.